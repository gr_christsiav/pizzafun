<?php

/**
 * This file provides database connectivity.
 *
 * @package    Pizzafun
 * @subpackage Helperfunctions
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */

include_once('DB.php');

$conninfo = "mysql://root:@localhost/pizzafun";
$db = DB::connect($conninfo);
    
if (DB::isError($db)) {
    print $db->getMessage();
    exit;
}

$db->query("SET names utf8");

?>