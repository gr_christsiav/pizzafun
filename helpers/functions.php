<?php

/**
 * This file contains common functions used throughout the application.
 *
 * @package    Pizzafun
 * @subpackage Helperfunctions
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */

/**
 * Create landing page url 
 * @param int $newsletterid
 * @param int $imageid
 * @param int $position
 * @return string $landing_page
 */
function get_landingpage($newsletterid, $imageid, $position) {

	global $db;
	
	//GET LANDING PAGE
	//http://www.pizzafan.gr/pizza_offer/create/mail20062014?utm_source=newsletter&utm_medium=email&utm_campaign=mail20062014&utm_content=promo1
		    
	//get newsletter date
	$campaign_date_txt = '';
	$result_newsletter_date = $db->query("SELECT campaign_date FROM newsletters WHERE id = ".$newsletterid);
	while ($Row_newsletterdate = $result_newsletter_date->fetchRow(DB_FETCHMODE_ASSOC)) {
	
	       $campaign_date = $Row_newsletterdate['campaign_date'];
	       $campaign_date_txt = 'mail'.date("d",strtotime($campaign_date)).date("m",strtotime($campaign_date)).date("Y",strtotime($campaign_date)); 
	}
			
		
	$landing_page = '';
	$resultlanding = $db->query("SELECT alias FROM images WHERE id =".$imageid);
	while ($Rowlanding = $resultlanding->fetchRow(DB_FETCHMODE_ASSOC)) {
	       $landing_page = 'http://www.pizzafan.gr/'.$Rowlanding['alias'].'/create/'.$campaign_date_txt.'?utm_source=newsletter&utm_medium=email&utm_campaign='.$campaign_date_txt.'&utm_content=promo'.$position;
	}
	if (PEAR::isError($resultlanding)) {
	    die($resultlanding->getMessage());
	}
	//GET LANDING PAGE	

	return $landing_page;
}

/**
 * Sends html email (newsletter) to subscribers using swift mailer library 
 * @param string $email_subject
 * @param array $recipients
 * @return int $result
 */

function send_html_email($email_subject, $recipients) {

require_once ('../../libraries/swiftmailer/lib/swift_required.php');
$transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');

// Create the Mailer using your created Transport
$mailer = Swift_Mailer::newInstance($transport);

// Create a message
$message = Swift_Message::newInstance($email_subject)
  ->setContentType("text/html")
  ->setFrom(array('test@pizzafun.gr' => 'Pizza fun'))
  ->setTo(array('receiver@domain.org', 'other@domain.org' => 'A name'))
  ->setBody('Here is the message itself')
  ;

// Send the message
//$result = $mailer->send($message);
return 1; //$result;

}

?>