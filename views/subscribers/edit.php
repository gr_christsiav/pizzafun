<?php
/**
 * This file provides functionality for editing the current subscriber (customer).
 *
 * @package    Pizzafun
 * @subpackage Views-Subscribers
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */
?>
<?php
set_include_path(get_include_path() . PATH_SEPARATOR . '../../helpers');
set_include_path(get_include_path() . PATH_SEPARATOR . '../../language');
require_once('el.php'); 
?>

<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title><?php echo $APP_TITLE; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../../libraries/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../libraries/bootstrap/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../libraries/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../libraries/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style type="text/css" id="holderjs-style"></style>
  <style type="text/css"> #firstForm td { padding: 5px;}</style>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pizza Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../../dashboard.php"><?php echo $DASHBOARD; ?></a></li>
            <li><a href="../../help.php"><?php echo $HELP; ?></a></li>
	    <li><a href="#" onClick="javascript: window.location.href='../../index.php';"><?php echo $LOGOUT; ?></a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input class="form-control" placeholder="Search..." type="text">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="../../dashboard.php"><?php echo $OVERVIEW; ?></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="../newsletters/list.php"><?php echo $SIDEBAR_NEWSLETTERS; ?></a></li>
            <li><a href="../images/list.php"><?php echo $SIDEBAR_IMAGES; ?></a></li>
	    <li><a href="../offers/list.php"><?php echo $SIDEBAR_OFFERS; ?></a></li>
            <li class="active"><a href="list.php"><?php echo $SIDEBAR_CUSTOMERS; ?></a></li>
	    <li><a href="../groups/list.php"><?php echo $SIDEBAR_LISTS; ?></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="page-header"><?php echo $VIEW_SUBSCRIBERS; ?></h2>
	  
          <div class="table-responsive">
           
	   <?php 
	   
	   require_once 'HTML/QuickForm.php';
	   require_once("db_connect.php");
		
	   //---------------------LOAD DEFAULTS---------------------//
	     if (isset($_GET['id'])) $id=$_GET['id'];
	     if (isset($_POST['id'])) $id=$_POST['id'];
		
	     //fetch group name	
	     $groupid_list = array();	
	     $groups_list  = $db->query("SELECT * FROM groups ORDER BY name");
             while ($Rowgroups = $groups_list->fetchRow(DB_FETCHMODE_ASSOC)) {
	             $groupid_list[$Rowgroups['id']] = $Rowgroups['name'];
	     }
	     
	     $result = $db->query("SELECT * FROM subscribers WHERE id = ".$id);
	     while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		       $groupid     = $Row['groupid'];
		       $name        = $Row['name'];
		       $surname     = $Row['surname'];
		       $address     = $Row['address'];
		       $email       = $Row['email'];
		       $age         = $Row['age'];
		       $latitude    = $Row['latitude'];
		       $longitude   = $Row['longitude'];
	     }
	  //---------------------LOAD DEFAULTS---------------------//
		
	        // Instantiate the HTML_QuickForm object
	        $form = new HTML_QuickForm('firstForm');

		// Set defaults for the form elements
		$form->setDefaults(array(
			'id' => $id,
			'groupid' => $groupid,
			'name' => $name,
			'surname' => $surname,
			'address' => $address,
			'email' => $email,
			'age' => $age,
			'latitude' => $latitude,
			'longitude' => $longitude
			
		));

		// Add some elements to the form
		$form->addElement('header', null, 'Τροποποίηση στοιχείων πελάτη');
		$form->addElement('select', 'groupid', 'Λίστα:', $groupid_list);
		$form->addElement('text', 'name', 'Όνομα:', array('size' => 50, 'maxlength' => 255));
		$form->addElement('text', 'surname', 'Επώνυμο:', array('size' => 50, 'maxlength' => 255));
		$form->addElement('text', 'address', 'Διεύθυνση:', array('size' => 50, 'maxlength' => 255));
		$form->addElement('text', 'email', 'Email:', array('size' => 50, 'maxlength' => 255));
		$form->addElement('text', 'age', 'Ηλικία:', array('size' => 10, 'maxlength' => 10));
		$form->addElement('text', 'latitude', 'Latitude:', array('size' => 10, 'maxlength' => 10));
		$form->addElement('text', 'longitude', 'Longtitude:', array('size' => 10, 'maxlength' => 10));
		$form->addElement('static','info','','<iframe width="400" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q='.$latitude.','.$longitude.'&hl=es;z=17&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?q='.$latitude.','.$longitude.'&hl=es;z=17&amp;output=embed" style="color:#0000FF;text-align:left" target="_blank">See map bigger</a></small>');
		$form->addElement('hidden','id');
		$btnGroup[] = &$form->createElement('submit', null, 'Αποθήκευση');
		$btnGroup[] = &$form->createElement('button', null, 'Ακύρωση','onClick="window.location.href=\'list.php\'"');
		$form->addGroup($btnGroup);
		
		// Define filters and validation rules
		$form->applyFilter('name', 'trim');
		$form->addRule('name', 'Παρακαλώ δώστε όνομα στη λίστα χρηστών', 'required', null, 'client');
		$form->applyFilter('surname', 'trim');
		$form->addRule('surname', 'Παρακαλώ δώστε επώνυμο στη λίστα χρηστών', 'required', null, 'client');
		$form->applyFilter('address', 'trim');
		$form->addRule('address', 'Παρακαλώ δώστε διεύθυνση στη λίστα χρηστών', 'required', null, 'client');
		$form->applyFilter('email', 'trim');
		$form->addRule('email', 'Παρακαλώ δώστε email στη λίστα χρηστών', 'required', null, 'client');
		
		// Try to validate a form
		if ($form->validate()) {
		    
		    $form->freeze();
		    $submitted_fields = $form->exportValues('id,groupid,name,surname,address,email,age,latitude,longitude');
		    
		    //Save values to DB
		    $result = $db->query("UPDATE subscribers SET groupid = '".$submitted_fields['groupid']."', name = '".$submitted_fields['name']. "', surname = '".$submitted_fields['surname']."', address = '".$submitted_fields['address']."', email = '".$submitted_fields['email']."', age = ".$submitted_fields['age'].", latitude = '".$submitted_fields['latitude']."', longitude = '".$submitted_fields['longitude']."' WHERE id = ".$submitted_fields['id']);
		    if (PEAR::isError($result)) {
			die($result->getMessage());
		    }
		     echo 'Η αλλαγή πραγματοποιήθηκε επιτυχώς&nbsp;&nbsp;<a href="list.php">Επιστροφή</a>';
		     exit;
	
		}

		// Output the form
		$form->display();
		
		?>
	   
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../libraries/bootstrap/js/jquery.js"></script>
    <script src="../../libraries/bootstrap/js/bootstrap.js"></script>
    <script src="../../libraries/bootstrap/js/docs.js"></script>
  

<div data-original-title="Copy to clipboard" title="" style="position: absolute; left: 0px; top: -9999px; width: 15px; height: 15px; z-index: 999999999;" class="global-zeroclipboard-container" id="global-zeroclipboard-html-bridge">      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="global-zeroclipboard-flash-bridge" height="100%" width="100%">         <param name="movie" value="/assets/flash/ZeroClipboard.swf?noCache=1404936983864">         <param name="allowScriptAccess" value="sameDomain">         <param name="scale" value="exactfit">         <param name="loop" value="false">         <param name="menu" value="false">         <param name="quality" value="best">         <param name="bgcolor" value="#ffffff">         <param name="wmode" value="transparent">         <param name="flashvars" value="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com">         <embed src="libraries/bootstrap/ZeroClipboard.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" name="global-zeroclipboard-flash-bridge" allowscriptaccess="sameDomain" allowfullscreen="false" type="application/x-shockwave-flash" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com" scale="exactfit" height="100%" width="100%">                </object></div></body></html>

