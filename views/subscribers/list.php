<?php
/**
 * This file provides functionality for listing all currently saved subscribers (customers).
 *
 * @package    Pizzafun
 * @subpackage Views-Groups
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */
?>
<?php
set_include_path(get_include_path() . PATH_SEPARATOR . '../../helpers');
set_include_path(get_include_path() . PATH_SEPARATOR . '../../language');
require_once('el.php'); 
?>

<!DOCTYPE html>
<html lang="el"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title><?php echo $APP_TITLE; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../../libraries/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../libraries/bootstrap/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../libraries/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../libraries/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style type="text/css" id="holderjs-style"></style></head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pizza Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../../dashboard.php"><?php echo $DASHBOARD; ?></a></li>
            <li><a href="../../help.php"><?php echo $HELP; ?></a></li>
	    <li><a href="#" onClick="javascript: window.location.href='../../index.php';"><?php echo $LOGOUT; ?></a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input class="form-control" placeholder="Search..." type="text">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="../../dashboard.php"><?php echo $OVERVIEW; ?></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="../newsletters/list.php"><?php echo $SIDEBAR_NEWSLETTERS; ?></a></li>
            <li><a href="../images/list.php"><?php echo $SIDEBAR_IMAGES; ?></a></li>
	    <li><a href="../offers/list.php"><?php echo $SIDEBAR_OFFERS; ?></a></li>
            <li class="active"><a href="list.php"><?php echo $SIDEBAR_CUSTOMERS; ?></a></li>
	    <li><a href="../groups/list.php"><?php echo $SIDEBAR_LISTS; ?></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="page-header"><?php echo $VIEW_SUBSCRIBERS; ?></h2>

	   <div class="alert alert-info" role="alert"><?php echo $VIEW_SUBSCRIBERS_INFOLIST; ?></div>
	   <br/>
	   
	   <fieldset>
	   <form method="get" action="">
	   <legend>Φίλτρα</legend>
	   Λίστα : 
	   <select name="group_filter" id="group_filter">
		<option value="all">Όλα</option>
		<?php
		require_once("db_connect.php");
		
		$result = $db->query("SELECT id,name FROM groups ORDER BY name");
		while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		       
		       $group_id   = $Row['id'];
		       $group_name = $Row['name'];
		       
		       if (isset($_GET['group_filter'])) {
			   if ($group_id == $_GET['group_filter'])
			       echo '<option value="'.$group_id .'" selected>'.$group_name.'</option>';
		           else
			       echo '<option value="'.$group_id .'">'.$group_name.'</option>';
		       }
		       else
			   echo '<option value="'.$group_id .'">'.$group_name.'</option>';
		}       
		?>
		</select>
		<input type="submit" value="Go"/>
	  </form>
	  </fieldset>
	  <br/>
	  
          <a href="new.php" class="btn btn-success"><?php echo $ACTIONS_NEW; ?></a><br/><br/>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
		  <th><a href="list.php?sort=name">Λίστα<a/></th>
                  <th>Όνομα</th>
		  <th>Επώνυμο</th>
		  <th>Διεύθυνση</th>
		  <th>Email</th>
		  <th>Ηλικία</th>
		  <th>Ενέργειες</th>
                </tr>
              </thead>
              <tbody>
                <?php 
		
		$where = '';
		if (isset($_GET['group_filter']))
		    if ($_GET['group_filter'] != 'all')
			$where = ' WHERE groupid = "'.$_GET['group_filter'].'"';
		
		$result = $db->query("SELECT * FROM subscribers".$where);
		while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		       $id          = $Row['id'];
		       $groupid     = $Row['groupid'];
		       
		       $groupname = '';
		       $result_groups = $db->query("SELECT name FROM groups WHERE id = ".$groupid);
		       while ($Rowgroups = $result_groups->fetchRow(DB_FETCHMODE_ASSOC)) {
			      $groupname = $Rowgroups['name'];
		       }
		       
		       $name        = $Row['name'];
		       $surname     = $Row['surname'];
		       $address     = $Row['address'];
		       $email       = $Row['email'];
		       $age         = $Row['age'];
		       
		       echo '<tr>';
		       echo '<td>'.$id.'</td>';
		       echo '<td>'.$groupname.'</td>';
		       echo '<td>'.$name.'</td>';
		       echo '<td>'.$surname.'</td>';
		       echo '<td>'.$address.'</td>';
		       echo '<td>'.$email.'</td>';
		       echo '<td>'.$age.'</td>';
		       echo '<td>
		             <a href="edit.php?id='.$id.'"" class="btn btn-small btn-info">'.$ACTIONS_EDIT.'</a> 
			     <a href="delete.php?id='.$id.'" class="btn btn-danger remove_fields">'.$ACTIONS_DELETE.'</a>
			     </td>';
		       echo '</tr>';
	        }
    
		$result->free();

	  ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../libraries/bootstrap/js/jquery.js"></script>
    <script src="../../libraries/bootstrap/js/bootstrap.js"></script>
    <script src="../../libraries/bootstrap/js/docs.js"></script>
  

<div data-original-title="Copy to clipboard" title="" style="position: absolute; left: 0px; top: -9999px; width: 15px; height: 15px; z-index: 999999999;" class="global-zeroclipboard-container" id="global-zeroclipboard-html-bridge">      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="global-zeroclipboard-flash-bridge" height="100%" width="100%">         <param name="movie" value="/assets/flash/ZeroClipboard.swf?noCache=1404936983864">         <param name="allowScriptAccess" value="sameDomain">         <param name="scale" value="exactfit">         <param name="loop" value="false">         <param name="menu" value="false">         <param name="quality" value="best">         <param name="bgcolor" value="#ffffff">         <param name="wmode" value="transparent">         <param name="flashvars" value="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com">         <embed src="libraries/bootstrap/ZeroClipboard.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" name="global-zeroclipboard-flash-bridge" allowscriptaccess="sameDomain" allowfullscreen="false" type="application/x-shockwave-flash" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com" scale="exactfit" height="100%" width="100%">                </object></div></body></html>