<?php
/**
 * This file provides functionality for creating a new offer image.
 *
 * @package    Pizzafun
 * @subpackage Views-Images
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */
?>
<?php
set_include_path(get_include_path() . PATH_SEPARATOR . '../../helpers');
set_include_path(get_include_path() . PATH_SEPARATOR . '../../language');
require_once('el.php'); 
?>

<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title><?php echo $APP_TITLE; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../../libraries/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../libraries/bootstrap/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../libraries/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../libraries/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style type="text/css" id="holderjs-style"></style>
  <style type="text/css"> #firstForm td { padding: 5px;}</style>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pizza Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../../dashboard.php"><?php echo $DASHBOARD; ?></a></li>
            <li><a href="../../help.php"><?php echo $HELP; ?></a></li>
	    <li><a href="#" onClick="javascript: window.location.href='../../index.php';"><?php echo $LOGOUT; ?></a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input class="form-control" placeholder="Search..." type="text">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="../../dashboard.php"><?php echo $OVERVIEW; ?></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="../newsletters/list.php"><?php echo $SIDEBAR_NEWSLETTERS; ?></a></li>
	    <li class="active" ><a href="list.php"><?php echo $SIDEBAR_IMAGES; ?></a></li>
            <li><a href="../offers/list.php"><?php echo $SIDEBAR_OFFERS; ?></a></li>
            <li><a href="../subscribers/list.php"><?php echo $SIDEBAR_CUSTOMERS; ?></a></li>
	    <li><a href="../groups/list.php"><?php echo $SIDEBAR_LISTS; ?></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="page-header"><?php echo $VIEW_IMAGES; ?></h2>
	  
          <div class="table-responsive">
           
	   <?php 
	   
	   require_once 'HTML/QuickForm.php';
		
	        // Instantiate the HTML_QuickForm object
	        $form = new HTML_QuickForm('firstForm');

		// Add some elements to the form
		$form->addElement('header', null, $FORM_IMAGES_HEADERNEW);
		$form->addElement('file', 'image_url', $FORM_IMAGES_URL);
		$form->addElement('text', 'alias', $FORM_IMAGES_ALIAS,  array('size' => 50, 'maxlength' => 255));
		$btnGroup[] = &$form->createElement('submit', null, $BUTTON_SAVE);
		$btnGroup[] = &$form->createElement('button', null, $BUTTON_CANCEL,'onClick="window.location.href=\'list.php\'"');
		$form->addGroup($btnGroup);

		// Define filters and validation rules
		$max_size = 831072;
		$form->applyFilter('image_url', 'trim');
		$form->addRule('image_url', $RULE_IMAGEURL, 'uploadedfile');
		$form->addRule('image_url', $RULE_IMAGEURLSIZE,'maxfilesize',$max_size);
		$form->applyFilter('alias', 'trim');
		$form->addRule('alias', $RULE_IMAGEALIAS, 'required', null, 'client');

		// Try to validate a form
		if ($form->validate()) {
		    
		    $form->freeze();
		    $form->process('move_file'); 
		     echo $ACTIONS_NEW_MSG.'&nbsp;&nbsp;<a href="list.php">'.$RETURN.'</a>';
		     exit;
	
		}

		// Output the form
		$form->display();
		
		function move_file($data) {
		
			set_include_path(get_include_path() . PATH_SEPARATOR . '../../helpers');
			require_once("db_connect.php");
			
			global $form;

			// Remove backslashes and forward slashes from new filename
			$new_name = strtr($data['image_url']['name'],'/\\','');
			// Remove ".." from new filename
			$new_name = str_replace('..','',$new_name);

			$file =& $form->getElement('image_url');
			$alias=& $form->getElement('alias');
		  
			
		  
			//Save values to DB
			$result = $db->query("INSERT INTO images(image_url,alias) VALUES('$new_name','".$alias->_attributes['value']."')");
			if (PEAR::isError($result)) {
				die($result->getMessage());
			}
			
			if ($file->moveUploadedFile('../../uploads',$new_name)) {
				print "The file has been uploaded to /uploads/$new_name.";
			} else {
				print "The file could not be uploaded to /uploads/$new_name.";
			}
		}
		?>
	   
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../libraries/bootstrap/js/jquery.js"></script>
    <script src="../../libraries/bootstrap/js/bootstrap.js"></script>
    <script src="../../libraries/bootstrap/js/docs.js"></script>
  

<div data-original-title="Copy to clipboard" title="" style="position: absolute; left: 0px; top: -9999px; width: 15px; height: 15px; z-index: 999999999;" class="global-zeroclipboard-container" id="global-zeroclipboard-html-bridge">      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="global-zeroclipboard-flash-bridge" height="100%" width="100%">         <param name="movie" value="/assets/flash/ZeroClipboard.swf?noCache=1404936983864">         <param name="allowScriptAccess" value="sameDomain">         <param name="scale" value="exactfit">         <param name="loop" value="false">         <param name="menu" value="false">         <param name="quality" value="best">         <param name="bgcolor" value="#ffffff">         <param name="wmode" value="transparent">         <param name="flashvars" value="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com">         <embed src="libraries/bootstrap/ZeroClipboard.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" name="global-zeroclipboard-flash-bridge" allowscriptaccess="sameDomain" allowfullscreen="false" type="application/x-shockwave-flash" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com" scale="exactfit" height="100%" width="100%">                </object></div></body></html>


