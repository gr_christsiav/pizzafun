<?php
/**
 * This file provides functionality for sending a newsletter to a list of subscribers.
 *
 * @package    Pizzafun
 * @subpackage Views-Newsletters
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */
?>
<?php
set_include_path(get_include_path() . PATH_SEPARATOR . '../../helpers');
set_include_path(get_include_path() . PATH_SEPARATOR . '../../language');
require_once('el.php'); 
?>

<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title><?php echo $APP_TITLE; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../../libraries/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../libraries/bootstrap/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../libraries/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../libraries/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   <style type="text/css" id="holderjs-style"></style>
   <style type="text/css"> #firstForm td { padding: 5px;}</style>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pizza Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../../dashboard.php"><?php echo $DASHBOARD; ?></a></li>
            <li><a href="../../help.php"><?php echo $HELP; ?></a></li>
	    <li><a href="#" onClick="javascript: window.location.href='../../index.php';"><?php echo $LOGOUT; ?></a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input class="form-control" placeholder="Search..." type="text">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="../../dashboard.php"><?php echo $OVERVIEW; ?></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li class="active"><a href="list.php"><?php echo $SIDEBAR_NEWSLETTERS; ?></a></li>
	    <li><a href="../images/list.php"><?php echo $SIDEBAR_IMAGES; ?></a></li>
            <li><a href="../offers/list.php"><?php echo $SIDEBAR_OFFERS; ?></a></li>
            <li><a href="../subscribers/list.php"><?php echo $SIDEBAR_CUSTOMERS; ?></a></li>
	    <li><a href="../groups/list.php"><?php echo $SIDEBAR_LISTS; ?></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="page-header"><?php echo $VIEW_NEWSLETTERS; ?></h2>
	  
	  <div class="alert alert-info" role="alert">Επιλέξτε τη λίστα χρηστών στην οποία θέλετε να αποσταλεί το ενημερωτικό δελτίο</div>
	  <br/>
	  
          <div class="table-responsive">
           
	   <?php 
	   
	   require_once 'HTML/QuickForm.php';
	   require_once("db_connect.php");
	
	   //---------------------LOAD DEFAULTS---------------------//
	     if (isset($_GET['id'])) $id=$_GET['id'];
	     if (isset($_POST['id'])) $id=$_POST['id'];
	     
	     $result = $db->query("SELECT * FROM newsletters WHERE id = ".$id);
	     while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		       //$id            = $Row['id'];
		       $subject       = $Row['subject'];
		       $author        = $Row['author'];
		       $created_at    = $Row['created_at'];
		       $campaign_date = $Row['campaign_date'];
	     }
	    
	     $group_list = array('0' => 'Όλες');
	     $result = $db->query("SELECT * FROM groups ORDER BY name");
	     while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		       $group_id          = $Row['id'];
		       $group_name        = $Row['name'];
		       $group_description = $Row['description'];
		       
		       $group_list[$group_id] = $group_name;
	     }
	     
	  //---------------------LOAD DEFAULTS---------------------//
	
	        // Instantiate the HTML_QuickForm object
	        $form = new HTML_QuickForm('firstForm');
		
		// Add some elements to the form
		$form->addElement('header', null, 'Αποστολή ενημερωτικού δελτίου');
		$form->addElement('text', 'subject', 'Θέμα:', array('size' => 50, 'maxlength' => 255, 'disabled' => 'disabled' , 'style' => 'background-color: #f7c3c3;'));
		$form->addElement('select', 'groupid', 'Λίστα:', $group_list);
		$form->addElement('hidden', 'id');
		$btnGroup[] = &$form->createElement('submit', null, 'Αποστολή');
		$btnGroup[] = &$form->createElement('button', null, 'Ακύρωση','onClick="window.location.href=\'list.php\'"');
		$form->addGroup($btnGroup);
		
		$form->setDefaults(array(
			'id' => $id,
			'subject' => $subject,
			'groupid' => $group_list
		));

		// Try to validate a form
		if ($form->validate()) {
		    
		    $form->freeze();
		    $submitted_fields = $form->exportValues('id,subject,groupid');
		    
		    //fetch all subscribers in the current group
		    $recipients = array();
		    $result_subscribers = $db->query("SELECT email FROM subscribers WHERE groupid = ". $submitted_fields['groupid']);
	            while ($Row_subscribers =  $result_subscribers->fetchRow(DB_FETCHMODE_ASSOC)) {
		          
			   $subscribers_email = $Row_subscribers['email'];
		           array_push($recipients, $subscribers_email);
	            }
		    
		    //Swift mailer
		    require_once("../../helpers/functions.php");
		    $email_status = send_html_email($subject, $recipients);
		    if ($email_status == 1)
		    {	echo '<div class="alert alert-success">
			      <strong>Επιτυχία!</strong> Το ενημερωτικό δελτίο απεστάλει στους χρήστες της λίστας&nbsp;&nbsp;<a href="list.php">Επιστροφή</a>
			      </div>';
		    }
		    else 
		    {   echo '<div class="alert alert-warning">
			      <strong>Σφάλμα!</strong> Το ενημερωτικό δελτίο δεν απεστάλει στους χρήστες της λίστας&nbsp;&nbsp;<a href="list.php">Επιστροφή</a>
			      </div>';
		    }
		    exit;
		}

		// Output the form
		$form->display();
		
		?>
	   
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../libraries/bootstrap/js/jquery.js"></script>
    <script src="../../libraries/bootstrap/js/bootstrap.js"></script>
    <script src="../../libraries/bootstrap/js/docs.js"></script>
  

<div data-original-title="Copy to clipboard" title="" style="position: absolute; left: 0px; top: -9999px; width: 15px; height: 15px; z-index: 999999999;" class="global-zeroclipboard-container" id="global-zeroclipboard-html-bridge">      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="global-zeroclipboard-flash-bridge" height="100%" width="100%">         <param name="movie" value="/assets/flash/ZeroClipboard.swf?noCache=1404936983864">         <param name="allowScriptAccess" value="sameDomain">         <param name="scale" value="exactfit">         <param name="loop" value="false">         <param name="menu" value="false">         <param name="quality" value="best">         <param name="bgcolor" value="#ffffff">         <param name="wmode" value="transparent">         <param name="flashvars" value="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com">         <embed src="libraries/bootstrap/ZeroClipboard.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" name="global-zeroclipboard-flash-bridge" allowscriptaccess="sameDomain" allowfullscreen="false" type="application/x-shockwave-flash" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com" scale="exactfit" height="100%" width="100%">                </object></div></body></html>


