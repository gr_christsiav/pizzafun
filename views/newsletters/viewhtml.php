<?php
/**
 * This file provides functionality for editing & viewing newsletter HTML code.
 *
 * @package    Pizzafun
 * @subpackage Views-Newsletters
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */
?>
<?php
set_include_path(get_include_path() . PATH_SEPARATOR . '../../helpers');
set_include_path(get_include_path() . PATH_SEPARATOR . '../../language');
require_once('el.php'); 
?>

<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title><?php echo $APP_TITLE; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../../libraries/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../libraries/bootstrap/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../libraries/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../libraries/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style type="text/css" id="holderjs-style"></style>
  <style type="text/css"> #firstForm td { padding: 5px;}</style>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pizza Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../../dashboard.php"><?php echo $DASHBOARD; ?></a></li>
            <li><a href="../../help.php"><?php echo $HELP; ?></a></li>
	    <li><a href="#" onClick="javascript: window.location.href='../../index.php';"><?php echo $LOGOUT; ?></a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input class="form-control" placeholder="Search..." type="text">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="../../dashboard.php"><?php echo $OVERVIEW; ?></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li class="active"><a href="list.php"><?php echo $SIDEBAR_NEWSLETTERS; ?></a></li>
            <li><a href="../images/list.php"><?php echo $SIDEBAR_IMAGES; ?></a></li>
	    <li><a href="../offers/list.php"><?php echo $SIDEBAR_OFFERS; ?></a></li>
            <li><a href="../subscribers/list.php"><?php echo $SIDEBAR_CUSTOMERS; ?></a></li>
	    <li><a href="../groups/list.php"><?php echo $SIDEBAR_LISTS; ?></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="page-header"><?php echo $VIEW_NEWSLETTERS; ?></h2>
	  
          <div class="table-responsive">
           
	   <script type="text/javascript" src="../../libraries/tinymce/tinymce.min.js"></script>
		<script type="text/javascript">
		tinymce.init({
			selector: "textarea",
			plugins: [
				"advlist autolink lists link image charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste"
			],
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
		});
		</script>
	   
		<?php 
	   
		require_once 'HTML/QuickForm.php';
		require_once("db_connect.php");
	
	        // Instantiate the HTML_QuickForm object
	        $form = new HTML_QuickForm('firstForm');
		
		// Add some elements to the form
		$form->addElement('header', null, 'Κώδικας ενημερωτικού δελτίου');
		$form->addElement('textarea', 'html_code', 'HTML κώδικας:', array('style' => 'height: 500px'));
		$form->addElement('hidden', 'id');
		$btnGroup[] = &$form->createElement('submit', null, 'Αποθήκευση');
		$btnGroup[] = &$form->createElement('button', null, 'Ακύρωση','onClick="window.location.href=\'list.php\'"');
		$form->addGroup($btnGroup);
		
		$HTMLtermplate_str = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Facebook sharing information tags -->
		<meta property="og:title" content="*|MC:SUBJECT|*">
			
		<title>
		*|MC:SUBJECT|*
		</title>
        
		<style type="text/css">
		.preheaderContent div a:visited {
			color: #336699; font-weight: normal; text-decoration: underline;
		}
		.headerContent a:visited {
			color: #336699; font-weight: normal; text-decoration: underline;
		}
		.bodyContent div a:visited {
			color: #336699; font-weight: normal; text-decoration: underline;
		}
		.footerContent div a:visited {
			color: #336699; font-weight: normal; text-decoration: underline;
		}
		</style>
		</head>
		<body leftmargin="0" topmargin="0" offset="0" style="width: 100% !important; -webkit-text-size-adjust: none; background: #FAFAFA; margin: 0; padding: 0;" bgcolor="#FAFAFA" marginheight="0" marginwidth="0">
			
		<center>
		<table id="backgroundTable" style="height: 100% !important; width: 100% !important; background: #FAFAFA; margin: 0; padding: 0;" cellpadding="0" cellspacing="0" bgcolor="#FAFAFA" border="0" height="100%" width="100%">
		<tbody>
			<tr>
			<td style="border-collapse: collapse;" align="center" valign="top">
				<table id="templateContainer" style="background: #FFFFFF; border: 1px solid #dddddd;" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" border="0" width="600">
				<tbody>
				<tr>
					<td style="border-collapse: collapse;" align="center" valign="top">
					<!-- // Begin Template Header \\ -->
					<table id="templateHeader" style="border-bottom-width: 0; background: #FFFFFF;" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" border="0" width="600">
					<tbody>
					<tr>
					<td class="headerContent" style="border-collapse: collapse; color: #202020; font-family: Arial; font-size: 14px; font-weight: normal; line-height: 120%; text-align: center; vertical-align: middle; padding: 0;" align="center" valign="middle">
					<!-- // Begin Module: Standard Header Image \\ -->
                                        <img src="../../uploads/banner_c.jpg" style="max-width: 600px !important; height: auto; line-height: 100%; outline: none; text-decoration: none; border: 0;" id="headerImage" mc:label="header_image" mc:edit="header_image" mc:allowdesigner="" mc:allowtext="">
                                        <div>Μοναδικές προσφορές &amp; επιπλέον 5% επιστροφή χρημάτων για την επόμενη παραγγελία
                                        </div><!-- // End Module: Standard Header Image \\ -->
					</td>
					</tr>
					</tbody>
					</table>
					<!-- // End Template Header \\ -->
					</td>
				</tr>
				<tr>
					<td style="border-collapse: collapse;" align="center" valign="top">
					<!-- // Begin Template Body \\ -->
					<table id="templateBody" cellpadding="0" cellspacing="0" border="0" width="600">
					<tbody>
					<tr>
					<td class="bodyContent" style="border-collapse: collapse; background: #FFFFFF;" bgcolor="#FFFFFF" valign="top">
                                        <!-- // Begin Module: Standard Content \\ -->
                                        <table cellpadding="20" cellspacing="0" border="0" width="100%">
                                                <tbody>
						<tr>
                                                <td style="border-collapse: collapse;" valign="top">
                                                <div mc:edit="std_content00" style="color: #505050; font-family: Arial; font-size: 14px; line-height: 150%; text-align: left;" align="left">';
		
						require_once("db_connect.php");
						require_once("functions.php");
						
						$id = $_GET['id'];
						
						$result = $db->query("SELECT * FROM offers WHERE newsletterid = ".$id. ' ORDER BY position');
						while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		
						       $nid         = $Row['id'];
						       $imageid     = $Row['imageid'];
						       $text_legend = $Row['text_legend'];		    
						     //$text_link   = $Row['text_link'];
						       $position    = $Row['position'];
						       $text_link   = get_landingpage($id, $imageid, $position);
		      
						       if ($imageid != NULL) {
						       
							   //FETCH IMAGE
						             $image_url  = '';
			                                     $resultimage = $db->query("SELECT image_url FROM images WHERE id = ".$imageid);
			                                     while ($Rowimage = $resultimage->fetchRow(DB_FETCHMODE_ASSOC)) {
				                                    $image_url = '../../uploads/'.$Rowimage['image_url'];
			                                     }
						             $resultimage->free();
		                                           //FETCH IMAGE
		       
							   $HTMLtermplate_str .= '
							   <!-- One -->
							   <a href="'.$text_link.'" target="_self" style="color: #336699; font-weight: normal; text-decoration: underline;">
                                                                <img src="'.$image_url.'" style="width: 600px; height: auto; line-height: 100%; outline: none; text-decoration: none; display: inline; border: 0;" align="none" width="600">
                                                           </a>
                                                           <div class="tpl-content-highlight" style="text-align: left; float: left; color: #505050; font-family: Arial; font-size: 14px; line-height: 150%;" align="left">
                                                                '.$text_legend.'
                                                           </div>
                                                           <div class="tpl-content-highlight" style="text-align: right; margin-right: 20px; color: #505050; font-family: Arial; font-size: 14px; line-height: 150%;" align="right">
                                                                <a href="'.$text_link.'" target="_self" style="color: #336699; font-weight: normal; text-decoration: underline;">TΑ ΘΕΛΩ ΟΛΑ</a>
                                                           </div>
							   <br>
                                                           <br>';
						        }
						}
    
						$result->free();
						
		
						$HTMLtermplate_str .= ' 
							<!-- End -->
                                                </div>
                                                </td>
                                                </tr>
                                                </tbody>
					</table><!-- // End Module: Standard Content \\ -->
                                        </td>
                                        </tr>
                                        </tbody>
					</table><!-- // End Template Body \\ -->
					</td>
				</tr>
				<tr>
                                <td style="border-collapse: collapse;" align="center" valign="top">
                                    <!-- // Begin Template Footer \\ -->
                                    <table id="templateFooter" style="border-top-width: 0; background: #FFFFFF;" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF" border="0" width="600">
                                        <tbody>
					<tr>
                                            <td class="footerContent" style="border-collapse: collapse;" valign="top">
                                            <!-- // Begin Module: Standard Footer \\ -->
                                            <table cellpadding="10" cellspacing="0" border="0" width="100%">
                                            <tbody>
						<tr>
                                                <td colspan="2" id="social" style="border-collapse: collapse; background: #FAFAFA; border: 0;" bgcolor="#FAFAFA" valign="middle">
                                                        <div mc:edit="std_social" style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;" align="center">
                                                                <a href="http://www.youtube.com/pizzafangreece" style="color: #336699; font-weight: normal; text-decoration: underline;">Δες το κανάλι μας στο YouTube</a> | <a href="http://www.facebook.com/www.pizzafan.gr" style="color: #336699; font-weight: normal; text-decoration: underline;">Γίνε fan στο Facebook</a> | <a href="http://www.pizzafan.gr/images/newsletter/*%7CFORWARD%7C*" style="color: #336699; font-weight: normal; text-decoration: underline;">Στείλε το σε ένα φίλο</a>
                                                        </div>
                                                        <div mc:edit="std_footer" style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;" align="center">
                                                                <em>© *|CURRENT_YEAR|* Pizza Fan</em>
                                                        </div>
                                                </td>
                                                </tr>
                                                <tr>
                                                       <td colspan="2" id="utility" style="border-collapse: collapse; background: #FFFFFF; border: 0;" bgcolor="#FFFFFF" valign="middle">
                                                          <div mc:edit="std_utility" style="color: #707070; font-family: Arial; font-size: 12px; line-height: 125%; text-align: center;" align="center">
                                                             Για να μη λαμβάνεις email με προσφορές <a href="http://www.pizzafan.gr/images/newsletter/*%7CUNSUB%7C*" style="color: #336699; font-weight: normal; text-decoration: underline;">κάνε κλικ εδώ</a><br>
                                                          <a href="http://www.pizzafan.gr/images/newsletter/*%7CUPDATE_PROFILE%7C*" style="color: #336699; font-weight: normal; text-decoration: underline;">Ρυθμίσεις εγγραφής</a>
                                                          </div>
                                                       </td>
                                                </tr>
                                              </tbody>
					      </table>
					      <!-- // End Module: Standard Footer \\ -->
                                            </td>
                                        </tr>
                                        </tbody>
				   </table>
				   <!-- // End Template Footer \\ -->
                                </td>
                                </tr>
				</tbody>
				</table>
				<br>
                        </td>
                        </tr>
                </tbody>
	        </table>
                </center>
            </body>
	    </html>';
		
		$form->setDefaults(array('html_code' => $HTMLtermplate_str));

		// Define filters and validation rules
		$form->applyFilter('name', 'trim');
		$form->addRule('name', 'Παρακαλώ δώστε όνομα στη λίστα χρηστών', 'required', null, 'client');

		// Try to validate a form
		if ($form->validate()) {
		    
		    $form->freeze();
		    $submitted_fields = $form->exportValues('subject,author,created_at,campaign_date');
		    
		    //Save values to DB
		    //echo "INSERT INTO newsletters(subject,author,created_at,campaign_date) VALUES('".$submitted_fields['subject']. "', '".$submitted_fields['author']."', '".$submitted_fields['created_at']."','".$submitted_fields['campaign_date']."')";
		    $created_at_value =  $submitted_fields['created_at']['Y']."-".$submitted_fields['created_at']['m']."-".$submitted_fields['created_at']['d']." ".$submitted_fields['created_at']['H'].":".$submitted_fields['created_at']['i'].":".$submitted_fields['created_at']['s'];
		    $result = $db->query("INSERT INTO newsletters(subject,author,created_at,campaign_date) VALUES('".$submitted_fields['subject']. "', '".$submitted_fields['author']."', '".$created_at_value."','".$submitted_fields['campaign_date']."')");
		    if (PEAR::isError($result)) {
			die($result->getMessage());
		    }
		     echo 'Η εγγραφή προστέθηκε επιτυχώς&nbsp;&nbsp;<a href="list.php">Επιστροφή</a>';
		     exit;
	
		}

		// Output the form
		$form->display();
		
		?>

          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../libraries/bootstrap/js/jquery.js"></script>
    <script src="../../libraries/bootstrap/js/bootstrap.js"></script>
    <script src="../../libraries/bootstrap/js/docs.js"></script>
  

<div data-original-title="Copy to clipboard" title="" style="position: absolute; left: 0px; top: -9999px; width: 15px; height: 15px; z-index: 999999999;" class="global-zeroclipboard-container" id="global-zeroclipboard-html-bridge">      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="global-zeroclipboard-flash-bridge" height="100%" width="100%">         <param name="movie" value="/assets/flash/ZeroClipboard.swf?noCache=1404936983864">         <param name="allowScriptAccess" value="sameDomain">         <param name="scale" value="exactfit">         <param name="loop" value="false">         <param name="menu" value="false">         <param name="quality" value="best">         <param name="bgcolor" value="#ffffff">         <param name="wmode" value="transparent">         <param name="flashvars" value="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com">         <embed src="libraries/bootstrap/ZeroClipboard.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" name="global-zeroclipboard-flash-bridge" allowscriptaccess="sameDomain" allowfullscreen="false" type="application/x-shockwave-flash" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com" scale="exactfit" height="100%" width="100%">                </object></div></body></html>


