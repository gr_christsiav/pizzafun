<?php
/**
 * This file provides functionality for editing the current newsletter.
 *
 * @package    Pizzafun
 * @subpackage Views-Newsletters
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */
?>
<?php
set_include_path(get_include_path() . PATH_SEPARATOR . '../../helpers');
set_include_path(get_include_path() . PATH_SEPARATOR . '../../language');
require_once('el.php'); 
?>

<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title><?php echo $APP_TITLE; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../../libraries/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../../libraries/bootstrap/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../libraries/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../libraries/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style type="text/css" id="holderjs-style"></style>
  <style type="text/css"> #firstForm td { padding: 5px;}</style></head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pizza Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="../../dashboard.php"><?php echo $DASHBOARD; ?></a></li>
            <li><a href="../../help.php"><?php echo $HELP; ?></a></li>
	    <li><a href="#" onClick="javascript: window.location.href='../../index.php';"><?php echo $LOGOUT; ?></a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input class="form-control" placeholder="Search..." type="text">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="../../dashboard.php"><?php echo $OVERVIEW; ?></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li class="active"><a href="list.php"><?php echo $SIDEBAR_NEWSLETTERS; ?></a></li>
	    <li><a href="../images/list.php"><?php echo $SIDEBAR_IMAGES; ?></a></li>
            <li><a href="../offers/list.php"><?php echo $SIDEBAR_OFFERS; ?></a></li>
            <li><a href="../subscribers/list.php"><?php echo $SIDEBAR_CUSTOMERS; ?></a></li>
	    <li><a href="../groups/list.php"><?php echo $SIDEBAR_LISTS; ?></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="page-header"><?php echo $VIEW_NEWSLETTERS; ?></h2>
	  
	  <div class="alert alert-info" role="alert"><?php echo $VIEW_NEWSLETTERS_INFOEDIT; ?></div>
	   <br/>
	  
          <div class="table-responsive">
           
	   <?php 
	   
	   require_once 'HTML/QuickForm.php';
	   require_once("db_connect.php");
	
	   //---------------------LOAD DEFAULTS---------------------//
	     if (isset($_GET['id'])) $id=$_GET['id'];
	     if (isset($_POST['id'])) $id=$_POST['id'];
	     
	     $result = $db->query("SELECT * FROM newsletters WHERE id = ".$id);
	     while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		       //$id            = $Row['id'];
		       $subject       = $Row['subject'];
		       $author        = $Row['author'];
		       $created_at    = $Row['created_at'];
		       $campaign_date = $Row['campaign_date'];
	     }
	  //---------------------LOAD DEFAULTS---------------------//
	
	        // Instantiate the HTML_QuickForm object
	        $form = new HTML_QuickForm('firstForm');
		
		// Add some elements to the form
		$form->addElement('header', null, $FORM_NEWSLETTER_HEADEREDIT);
		$form->addElement('text', 'subject', $FORM_NEWSLETTER_TITLE, array('size' => 50, 'maxlength' => 255));
		$form->addElement('text', 'author', $FORM_NEWSLETTER_USER, array('size' => 50, 'maxlength' => 255, 'disabled', 'style'=>'background-color: #f7c3c3'));
		$form->addElement('text', 'created_at', $FORM_NEWSLETTER_CREATEDAT, array('size' => 50, 'maxlength' => 255, 'disabled' , 'style'=>'background-color: #f7c3c3'));
		$form->addElement('date', 'campaign_date', $FORM_NEWSLETTER_CAMPAIGN ,array('language' => 'el', 'format' => 'd-m-Y H:i:s','minYear' => 2014, 'maxYear' => date('Y') + 2));
		$form->addElement('hidden', 'id');
		$btnGroup[] = &$form->createElement('submit', null, $BUTTON_SAVE);
		$btnGroup[] = &$form->createElement('button', null, $BUTTON_CANCEL,'onClick="window.location.href=\'list.php\'"');
		$form->addGroup($btnGroup);
		
		$form->setDefaults(array(
			'id' => $id,
			'subject' => $subject,
			'author' => $author,
			'created_at' => $created_at,
			'campaign_date' => array('Y' => date("Y",strtotime($campaign_date)), 'm' => date("m",strtotime($campaign_date)), 'd' => date("d",strtotime($campaign_date)), 'H' => date("H",strtotime($campaign_date)), 'i' => date("i",strtotime($campaign_date)), 's' => date("s",strtotime($campaign_date)))
		));

		// Define filters and validation rules
		$form->applyFilter('name', 'trim');
		$form->addRule('name', $RULE_NEWSLETTER, 'required', null, 'client');

		// Try to validate a form
		if ($form->validate()) {
		    
		    $form->freeze();
		    $submitted_fields = $form->exportValues('id,subject,author,created_at,campaign_date');
		    
		    //Save values to DB
		    $campaign_date_value = $submitted_fields['campaign_date']['Y'].'-'.$submitted_fields['campaign_date']['m'].'-'.$submitted_fields['campaign_date']['d'].' '.$submitted_fields['campaign_date']['H'].':'.$submitted_fields['campaign_date']['i'].':'.$submitted_fields['campaign_date']['s'];
		    $result = $db->query("UPDATE newsletters SET subject = '".$submitted_fields['subject']."',author = '".$submitted_fields['author']."',created_at = '".$submitted_fields['created_at']."', campaign_date = '".$campaign_date_value. "' WHERE id = ".$submitted_fields['id']);
		    if (PEAR::isError($result)) {
			die($result->getMessage());
		    }
		     echo $ACTIONS_EDIT_MSG.'&nbsp;&nbsp;<a href="list.php">'.$RETURN.'</a>';
		     exit;
	
		}

		// Output the form
		$form->display();
	
		?>
		
		<br/>
		<!------------------Display current newsletter offers ------------------->
		<div id="accordion" class="panel-group">

		<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><?php echo $ROW_OFFERS; ?></a>
			</h4>
		</div>

		<div id="collapseFour" class="panel-collapse collapse in">
			<div class="panel-body">
			<div class="table-responsive">
			<table class="table table-striped">
			<thead>
			<tr>
			 <th>#</th>
                  <th><?php echo $ROW_IMAGE; ?></th>
                  <th><?php echo $ROW_LEGEND; ?></th>
		  <th><?php echo $ROW_POSITION; ?></th>
		  <th><?php echo $ROW_ACTIONS; ?></th>
			</tr>
			</thead>
			<tbody>
                
			<?php
			$result = $db->query("SELECT * FROM offers WHERE newsletterid = ".$id);
			while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		
			       $id            = $Row['id'];
		               $newsletterid  = $Row['newsletterid'];
		               $imageid       = $Row['imageid'];
		               $text_legend   = $Row['text_legend'];
		               $position      = $Row['position'];
		       
			       //FETCH IMAGE
				 $image_url  = '';
			         $resultimage = $db->query("SELECT image_url FROM images WHERE id = ".$imageid);
			         while ($Rowimage = $resultimage->fetchRow(DB_FETCHMODE_ASSOC)) {
				        $image_url = $Rowimage['image_url'];
			         }
		              //FETCH IMAGE
		       
			      echo '<tr>';
		              echo '<td>'.$id.'</td>';
		              echo '<td><a href="'.$image_url.'" target="_blank"><img src="../../uploads/'.$image_url.'" style="width:200px"/></a></td>';
		              echo '<td>'.$text_legend.'</td>';
		              echo '<td>'.$position.'</td>';
		              echo '<td>
			            <a href="../offers/edit.php?id='.$id.'"" class="btn btn-small btn-info">'.$ACTIONS_EDIT.'</a> 
			            <a href="../offers/delete.php?id='.$id.'" class="btn btn-danger remove_fields">'.$ACTIONS_DELETE.'</a>
		                   </td>';
		              echo '</tr>';
			}
    
			$result->free();
			
			?>
           
			</tbody>
			</table>
			</div>    
			</div>
		</div>
		</div>
		<!------------------Display current newsletter offers -------------------> 
	  
        </div>
	  
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../../libraries/bootstrap/js/jquery.js"></script>
    <script src="../../libraries/bootstrap/js/bootstrap.js"></script>
    <script src="../../libraries/bootstrap/js/docs.js"></script>
  

<div data-original-title="Copy to clipboard" title="" style="position: absolute; left: 0px; top: -9999px; width: 15px; height: 15px; z-index: 999999999;" class="global-zeroclipboard-container" id="global-zeroclipboard-html-bridge">      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="global-zeroclipboard-flash-bridge" height="100%" width="100%">         <param name="movie" value="/assets/flash/ZeroClipboard.swf?noCache=1404936983864">         <param name="allowScriptAccess" value="sameDomain">         <param name="scale" value="exactfit">         <param name="loop" value="false">         <param name="menu" value="false">         <param name="quality" value="best">         <param name="bgcolor" value="#ffffff">         <param name="wmode" value="transparent">         <param name="flashvars" value="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com">         <embed src="libraries/bootstrap/ZeroClipboard.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" name="global-zeroclipboard-flash-bridge" allowscriptaccess="sameDomain" allowfullscreen="false" type="application/x-shockwave-flash" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com" scale="exactfit" height="100%" width="100%">                </object></div></body></html>


