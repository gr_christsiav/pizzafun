-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 17, 2014 at 10:34 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pizzafun`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(2, 'Κυψέλη', 'Αυτή η λίστα περιέχει τους χρήστες που διαμένουν στην περιοχή της Κυψέλης'),
(3, 'Πατήσια', 'Αυτή η λίστα περιέχει τους χρήστες που διαμένουν στα Άνω Πατήσια'),
(5, 'Άγιος Ελευθέριος', 'Αυτή η λίστα περιέχει τους χρήστες που κατοικούν στον Άγιο Ελευθέριο.'),
(6, 'Εξάρχεια', 'Αυτή η λίστα περιέχει τους κατοίκους που κατοικούν στα Εξάρχεια.');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(255) NOT NULL COMMENT 'image url path in the filesystem',
  `alias` varchar(255) NOT NULL COMMENT 'landing page alias for this offer',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_url`, `alias`) VALUES
(6, 'o_green.jpg', 'pizza_offer'),
(7, 'o_.jpg', 'pizza_offer'),
(8, 'o_summer_large.jpg', 'pizza_offer'),
(9, 'po_basket.jpg', 'pizza_offer'),
(10, 'po_chocokrats.jpg', 'pizza_offer_chocokrats'),
(11, 'po_beer.jpg', 'pizza_offer_beer');

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL COMMENT 'Newsletter subject (for instance Weekly offer 7/7-11/7)',
  `author` varchar(255) NOT NULL COMMENT 'username that created the newsletter. Useful for statistical purposes ',
  `created_at` varchar(30) NOT NULL COMMENT 'Newslleter creation date (not the same as the campaign date. Useful for statistical purposes)',
  `campaign_date` datetime NOT NULL COMMENT 'When the newsletter is being sent to customers',
  `html_code` text NOT NULL COMMENT 'HTML code of the newsletter',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Table holding newsletter attributes' AUTO_INCREMENT=13 ;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `subject`, `author`, `created_at`, `campaign_date`, `html_code`) VALUES
(1, 'Προσφορές 7/7-13/7', 'test@pizzafun.gr', '2014-07-10 14:06:12', '2014-07-30 13:00:00', ''),
(2, 'Προσφορές 14/7-20/7', 'test@pizzafun.gr', '2014-07-12 00:00:00', '2014-02-02 14:00:00', ''),
(12, 'Προσφορές 28/7-3/8', 'test@pizzafun.gr', '2014-07-17 22:10:25', '2014-07-28 13:10:30', '');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `newsletterid` int(10) NOT NULL COMMENT 'the newsletter this offer is associated with',
  `imageid` int(10) NOT NULL COMMENT 'image used for this offer',
  `text_legend` varchar(255) NOT NULL COMMENT 'offer text message',
  `text_link` varchar(255) NOT NULL COMMENT 'url (link) of the offer shortened for keeping track of clicks',
  `position` int(10) NOT NULL COMMENT 'to the position of the offer in the message',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `newsletterid`, `imageid`, `text_legend`, `text_link`, `position`) VALUES
(1, 1, 6, '2 ΜΕΓΑΛΕΣ ΠΙΤΣΕΣ 8ΤΜΧ', 'http://www.pizzafan.gr/pizza_offer/create/mail30072014?utm_source=newsletter&utm_medium=email&utm_campaign=mail30072014&utm_content=promo2', 2),
(2, 1, 11, '2 ΜΕΓΑΛΕΣ ΠΙΤΣΕΣ 8ΤΜΧ + ΔΩΡΟ ΜΠΥΡΑ 330ML ΜΕ 10€', 'http://www.pizzafan.gr/pizza_offer_beer/create/mail30072014?utm_source=newsletter&utm_medium=email&utm_campaign=mail30072014&utm_content=promo1', 1),
(7, 1, 9, '2ΠΟΝΤΟ ΣΤΗ ΓΕΥΣΗ', 'http://www.pizzafan.gr/pizza_offer/create/mail30072014?utm_source=newsletter&utm_medium=email&utm_campaign=mail30072014&utm_content=promo3', 3),
(9, 12, 8, '2 ΜΕΓΑΛΕΣ ΠΙΣΤΕΣ 8τμχ', 'http://www.pizzafan.gr/pizza_offer/create/mail27072014?utm_source=newsletter&utm_medium=email&utm_campaign=mail27072014&utm_content=promo1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `offers2images`
--

CREATE TABLE IF NOT EXISTS `offers2images` (
  `imageid` int(11) NOT NULL AUTO_INCREMENT,
  `offerid` int(11) NOT NULL,
  PRIMARY KEY (`imageid`,`offerid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `offers2images`
--

INSERT INTO `offers2images` (`imageid`, `offerid`) VALUES
(1, 1),
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `groupid` int(10) NOT NULL COMMENT 'the user group this subscriber belongs to. Sometimes you may want to create multiple lists of subscribers (for instance Kypseli group, Ampelokipoi group, Patisia group)',
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL COMMENT 'Google maps latitude',
  `longitude` varchar(255) NOT NULL COMMENT 'Google maps longitude',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `groupid`, `name`, `surname`, `address`, `email`, `age`, `latitude`, `longitude`) VALUES
(1, 2, 'Μπάμπης', 'Ταβερνιάρης', 'Σταδίου, Αθήνα', 'mpampis@test.gr', 28, '37.98312', '23.72906'),
(2, 3, 'Χάρης', 'Παλιούσης', 'Κωσταντίνου Παλαιολόγου, Χαλάνδρι', 'xaris@test.gr', 23, '38.02148', '23.79320'),
(3, 2, 'Γιώργος', 'Χαριτοδιπλωμένος', 'Παξών 40, Αθήνα', 'giorgos@test.gr', 42, '37.99654', '23.74070'),
(4, 6, 'Βασίλης', 'Χαλιάτσος', 'Ζωοδόχου Πηγής 50', 'xaliatsos@test.gr', 52, '', ''),
(6, 5, 'Δημήτριος', 'Μπαλομένος', 'Αγησιλάου 35', 'mpalomenos@test.gr', 0, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
