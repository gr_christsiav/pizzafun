Requirements
------------
Βασική προϋπόθεση για να τρέξει η εφαρμογή είναι η εγκατάσταση του PEAR

Notes
-----
Βασικά έχω δημιουργήσει κατά κάποιον τρόπο το δικό μου mini-framework με το ακόλουθη οργάνωση αρχείων:

assets/
   css/
   images/
   js/
doc/   
helpers/
language/
libraries/
uploads/
views/
     groups/
     images/
     newsletters/
     offers/
     subscribers/
     
- Ο κατάλογος 'helpers' περιέχει βοηθητικά αρχεία με λειτουργικότητα όπως πχ commonly used functions, database connectivity κτλ
- Ο κατάλογος 'language' περιέχει τα λεκτικά (strings) του interface
- Ο κατάλογος 'libraries' περιέχει τις βιβλιοθήκες που χρησιμοποιούμε πχ swiftmailer,tinymce,twitter bootstrap κτλ
- Ο κατάλογος 'uploads' περιέχει τις αποθηκευμένες εικόνες των προσφορών που ανεβάζουν οι χρήστες
- Ο κατάλογος 'views' περιέχει ομαδοποιημένες ανά κατηγορία και λειτουργία τις φόρμες της εφαρμογής   

Οι βιβλιοθήκες που έχω χρησιμοποιήσει είναι:
- PEAR DB (για σύνδεση με τη βάση δεδομένων)
- PEAR QuickForm (για τις φόρμες, το validation κτλ)
- Twitter Bootstrap (σαν Responsive CSS/Layout framework)
- TinyMCE (σαν κειμενογράφο για το editing του newsletter)
- Swiftmailer (για την αποστολή των email)
- Σε αρχικό στάδιο χρησιμοποίησα (βοηθητικά) το phpscaffold.com για παραγωγή βασικού CRUD το οποίο στη συνέχεια το τροποποίησα προσθέτοντας το PEAR QuickForm

Υπάρχουν σε ορισμένα σημεία σχόλια υπό τη μορφή phpdoc.