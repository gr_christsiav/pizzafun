<?php

$APP_TITLE               = 'Pizza Fun - Newsletter App';
$DASHBOARD               = 'Dashboard';
$HELP                    = 'Βοήθεια';
$LOGOUT                  = 'Αποσύνδεση';
$OVERVIEW                = 'Dashboard';
$RETURN                  = 'Επιστροφή';

$BUTTON_SAVE             = 'Αποθήκευση';
$BUTTON_CANCEL           = 'Ακύρωση';

$ACTIONS_NEW             = 'Νέα εγγραφή';
$ACTIONS_NEW_MSG         = 'Η εγγραφή προστέθηκε επιτυχώς';
$ACTIONS_EDIT            = 'Τροποποίηση';
$ACTIONS_DELETE          = 'Διαγραφή';
$ACTIONS_EDIT_MSG        = 'Η αλλαγή πραγματοποιήθηκε με επιτυχία';
$ACTIONS_DELETE_MSG      = 'Η διαγραφή έγινε με επιτυχία';
$ACTIONS_VIEWHTML        = 'View HTML';
$ACTIONS_SEND            = 'Αποστολή';

$FORM_GROUP_HEADERNEW    = 'Προσθήκη λίστας';
$FORM_GROUP_HEADEREDIT   = 'Τροποποίηση στοιχείων λίστας';
$FORM_IMAGES_HEADERNEW   = 'Προσθήκη εικόνας';
$FORM_IMAGES_HEADEREDIT  = 'Τροποποίηση εικόνας';
$FORM_GROUPNAME          = 'Όνομα λίστας:';
$FORM_GROUPDESCRIPTION   = 'Περιγραφή:';
$FORM_IMAGES_URL         = 'Αρχείο:';
$FORM_IMAGES_ALIAS       = 'Alias:';
$FORM_NEWSLETTER_HEADEREDIT = 'Τροποποίηση ενημερωτικού δελτίου';
$FORM_NEWSLETTER_TITLE   = 'Θέμα:';
$FORM_NEWSLETTER_USER    = 'Χρήστης:';
$FORM_NEWSLETTER_CREATEDAT = 'Ημ. δημιουργίας:';
$FORM_NEWSLETTER_CAMPAIGN  = 'Ημ. αποστολής:';

$SIDEBAR_NEWSLETTERS     = 'Ενημερωτικά δελτία';
$SIDEBAR_IMAGES          = 'Εικόνες';
$SIDEBAR_CUSTOMERS       = 'Πελάτες';
$SIDEBAR_OFFERS          = 'Προσφορές';
$SIDEBAR_LISTS           = 'Λίστες χρηστών';

$PANEL_LATESTUSERS       = 'Τελευταίοι Χρήστες';
$PANEL_LATESTOFFERS      = 'Τελευταίες Προσφορές';
$PANEL_LATESTLISTS       = 'Τελευταίες Λίστες'; 
$PANEL_LATESTNEWSLETTERS = 'Τελευταία Newsletters';

$ROW_ACTIONS             = 'Ενέργειες';
$ROW_ADDRESS             = 'Διεύθυνση';
$ROW_AGE                 = 'Ηλικία';
$ROW_EMAIL               = 'Email';
$ROW_CAMPAIGNDATE        = 'Ήμ. αποστολής';
$ROW_CREATEDAT           = 'Ημ. δημιουργίας';
$ROW_SUBJECT             = 'Θέμα';
$ROW_USER                = 'Χρήστης';
$ROW_DESCRIPTION         = 'Περιγραφή';
$ROW_NAME                = 'Ονομασία';
$ROW_NEWSLETTER          = 'Newsletter';
$ROW_IMAGE               = 'Εικόνα';
$ROW_LEGEND              = 'Κείμενο';
$ROW_POSITION            = 'Θέση';
$ROW_LIST                = 'Λίστα';
$ROW_LASTNAME            = 'Επώνυμο';
$ROW_OFFERS              = 'Προσφορές';

$RULE_GROUPNAME          = 'Παρακαλώ δώστε όνομα στη λίστα χρηστών';
$RULE_IMAGEALIAS         = 'Παρακαλώ δώστε κάποιο ψευδώνυμο για αυτή την εικόνα';
$RULE_IMAGEURL           = 'Παρακαλώ ανεβάστε κάποιο αρχείο εικόνας';
$RULE_IMAGEURLSIZE       = 'Το αρχείο που ανεβάσατε είναι πολύ μεγάλο';
$RULE_NEWSLETTER         = 'Παρακαλώ δώστε τίτλο στο newsletter';

$VIEW_GROUPS             = 'Λίστες χρηστών';
$VIEW_GROUPS_INFO        = 'Στην ενότητα αυτή μπορείτε να δημιουργήσετε λίστες χρηστών (ηλεκτρονικού ταχυδρομείου). Μια λίστα μπορεί να διαχωρίζει τους χρήστες ανά περιοχή, νομό, χώρα κτλ. Αργότερα κατά την αποστολή του newsletter θα έχετε τη δυνατότητα να το στείλετε σε μια ή όλες τις λίστες χρηστών που δημιουργήσατε από αυτή την οθόνη';
$VIEW_IMAGES             = 'Εικόνες';
$VIEW_IMAGES_INFO        = 'Στην ενότητα αυτή ανεβάζετε τις εικόνες(αρχεία) των προσφορών';
$VIEW_NEWSLETTERS        = 'Ενημερωτικά δελτία';
$VIEW_NEWSLETTERS_INFOEDIT = 'Στην ενότητα αυτή μπορείτε να τροποποιήσετε ένα ενημερωτικό δελτίο (newsletter) καθώς και τις προσφορές που περιέχει';
$VIEW_NEWSLETTERS_INFOLIST = 'Στην ενότητα αυτή μπορείτε να δημιουργήσετε ενημερωτικά δελτία (newsletters)';
$VIEW_SUBSCRIBERS        = 'Πελάτες';
$VIEW_SUBSCRIBERS_INFOLIST = 'Η ενότητα αυτή περιέχει τους εγγεγραμμένους χρήστες (πελάτες). Κάθε πελάτης ανήκει σε μια λίστα που μπορεί να τον διαχωρίζει ανά περιοχή, ανά νομό κτλ';
$VIEW_OFFERS             = 'Προσφορές';
$VIEW_OFFERS_LIST        = 'Στην ενότητα αυτή δημιουργείτε προσφορές και τις συσχετίζετε με ενημερωτικά δελτία (newsletters)';
?>