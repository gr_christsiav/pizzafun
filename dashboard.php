<?php
/**
 * This file provides a dashboard with all the functions aggregated in one place.
 *
 * @package    Pizzafun
 * @subpackage Main
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */
?>
<?php require_once('language/el.php'); ?>

<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title><?php echo $APP_TITLE; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="libraries/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="libraries/bootstrap/css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="libraries/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="libraries/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  <style type="text/css" id="holderjs-style"></style></head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Pizza Fun</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="dashboard.php"><?php echo $DASHBOARD; ?></a></li>
            <li><a href="help.php"><?php echo $HELP; ?></a></li>
	    <li><a href="#" onClick="javascript: window.location.href='index.php';"><?php echo $LOGOUT; ?></a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input class="form-control" placeholder="Search..." type="text">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="#"><?php echo $OVERVIEW; ?></a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="views/newsletters/list.php"><?php echo $SIDEBAR_NEWSLETTERS; ?></a></li>
	    <li><a href="views/images/list.php"><?php echo $SIDEBAR_IMAGES; ?></a></li>
            <li><a href="views/offers/list.php"><?php echo $SIDEBAR_OFFERS; ?></a></li>
            <li><a href="views/subscribers/list.php"><?php echo $SIDEBAR_CUSTOMERS; ?></a></li>
	    <li><a href="views/groups/list.php"><?php echo $SIDEBAR_LISTS; ?></a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header"><?php echo $DASHBOARD; ?></h1>

	  <!------------------------------- LATEST USERS -------------------------------->
	  <div id="accordion" class="panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?php echo $PANEL_LATESTUSERS; ?></a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?php echo $ROW_LIST; ?></th>
                  <th><?php echo $ROW_NAME; ?></th>
                  <th><?php echo $ROW_LASTNAME; ?></th>
                  <th><?php echo $ROW_ADDRESS; ?></th>
		  <th><?php echo $ROW_EMAIL; ?></th>
		  <th><?php echo $ROW_AGE; ?></th>
		  <th><?php echo $ROW_ACTIONS; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php 
		
		set_include_path(get_include_path() . PATH_SEPARATOR . 'helpers');
		require_once("db_connect.php");
		
		$result = $db->query("SELECT * FROM subscribers ORDER BY id DESC LIMIT 5");
		while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		       $id            = $Row['id'];
		       $groupid       = $Row['groupid'];
		       $name          = $Row['name'];
		       $surname       = $Row['surname'];
		       $address       = $Row['address'];
		       $email         = $Row['email'];
		       $age           = $Row['age'];
		       
		       //FETCH LIST NAME
		         $groupname = '';
			 $resultgroup = $db->query("SELECT name FROM groups WHERE id = ".$groupid);
			 while ($Rowgroup = $resultgroup->fetchRow(DB_FETCHMODE_ASSOC)) {
				$groupname = $Rowgroup['name'];
			 }
		       //FETCH LIST NAME
		       
		       echo '<tr>';
		       echo '<td>'.$id.'</td>';
		       echo '<td>'.$groupname.'</td>';
		       echo '<td>'.$name.'</td>';
		       echo '<td>'.$surname.'</td>';
		       echo '<td>'.$address.'</td>';
		       echo '<td>'.$email.'</td>';
		       echo '<td>'.$age.'</td>';
		       echo '<td><a href="views/subscribers/edit.php?id='.$id.'"" class="btn btn-small btn-info">'.$ACTIONS_EDIT.'</a> 
			     <a href="views/subscribers/delete.php?id='.$id.'" class="btn btn-danger remove_fields">'.$ACTIONS_DELETE.'</a></td>';
		       echo '</tr>';
	        }
    
		$result->free();

		?>
               
              </tbody>
            </table>
          </div>   
		</div>
            </div>
          </div>
	  </div>
	  <!------------------------------- LATEST USERS -------------------------------->
          
	  
	<!-------------------------------- LATEST OFFERS -------------------------------->
	<div id="accordion" class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><?php echo $PANEL_LATESTOFFERS; ?></a>
                </h4>
            </div>

            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                 <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?php echo $ROW_NEWSLETTER; ?></th>
                  <th><?php echo $ROW_IMAGE; ?></th>
                  <th><?php echo $ROW_LEGEND; ?></th>
		  <th><?php echo $ROW_POSITION; ?></th>
		  <th><?php echo $ROW_ACTIONS; ?></th>
                </tr>
              </thead>
              <tbody>
               <?php 
	  
		set_include_path(get_include_path() . PATH_SEPARATOR . 'helpers');
		require_once("db_connect.php");
		
		$result = $db->query("SELECT * FROM offers ORDER BY id DESC LIMIT 5");
		while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		
		       $id            = $Row['id'];
		       $newsletterid  = $Row['newsletterid'];
		       $imageid       = $Row['imageid'];
		       $text_legend   = $Row['text_legend'];
		       $position      = $Row['position'];
		       
		       //FETCH LIST NAME
		         $newsletter_name = '';
			 $resultnewsletter = $db->query("SELECT subject FROM newsletters WHERE id = ".$newsletterid);
			 while ($Rownewsletter = $resultnewsletter->fetchRow(DB_FETCHMODE_ASSOC)) {
				$newsletter_name = $Rownewsletter['subject'];
			 }
		       //FETCH LIST NAME
		       
		       //FETCH IMAGE
		         $image_url  = '';
			 $resultimage = $db->query("SELECT image_url FROM images WHERE id = ".$imageid);
			 while ($Rowimage = $resultimage->fetchRow(DB_FETCHMODE_ASSOC)) {
				$image_url = $Rowimage['image_url'];
			 }
		       //FETCH IMAGE
		       
		       echo '<tr>';
		       echo '<td>'.$id.'</td>';
		       echo '<td>'.$newsletter_name.'</td>';
		       echo '<td><a href="'.$image_url.'" target="_blank"><img src="uploads/'.$image_url.'" style="width:200px"/></a></td>';
		       echo '<td>'.$text_legend.'</td>';
		       echo '<td>'.$position.'</td>';
		       echo '<td><a href="views/offers/edit.php?id='.$id.'"" class="btn btn-small btn-info">'.$ACTIONS_EDIT.'</a> 
			     <a href="views/offers/delete.php?id='.$id.'" class="btn btn-danger remove_fields">'.$ACTIONS_DELETE.'</a></td>';
		       echo '</tr>';
	        }
    
		$result->free();

	  ?>
               
              </tbody>
            </table>
          </div>  
		</div>
            </div>
        </div>
	</div>
	<!------------------------------ LATEST OFFERS -------------------------------->
	
	<!-- <h2 class="sub-header">Τελευταίες Προσφορές</h2>-->
          
	<!------------------------------- LATEST LISTS -------------------------------->
	  
	<div id="accordion" class="panel-group">
	<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><?php echo $PANEL_LATESTLISTS; ?></a>
                </h4>
            </div>

            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th><?php echo $ROW_NAME; ?></th>
                  <th><?php echo $ROW_DESCRIPTION; ?></th>
                </tr>
              </thead>
              <tbody>
                 <?php 
	  
		set_include_path(get_include_path() . PATH_SEPARATOR . 'helpers');
		require_once("db_connect.php");
		
		$result = $db->query("SELECT * FROM groups ORDER BY id DESC LIMIT 5");
		while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		       $id          = $Row['id'];
		       $name        = $Row['name'];
		       $description = $Row['description'];
		       
		       echo '<tr>';
		       echo '<td>'.$id.'</td>';
		       echo '<td>'.$name.'</td>';
		       echo '<td>'.$description.'</td>';
		       echo '<td><a href="views/groups/edit.php?id='.$id.'"" class="btn btn-small btn-info">'.$ACTIONS_EDIT.'</a> 
			     <a href="views/groups/delete.php?id='.$id.'" class="btn btn-danger remove_fields">'.$ACTIONS_DELETE.'</a></td>';
		       echo '</tr>';
	        }
    
		$result->free();

	  ?>
               
              </tbody>
            </table>
          </div>
		</div>
            </div>
        </div>
	</div>
	<!------------------------------- LATEST LISTS -------------------------------->
	
	  <!--<h2 class="sub-header">Τελευταίες Λίστες</h2>-->
	  
	  <!------------------------- LATEST NEWSLETTERS ------------------------------>   

    <div id="accordion" class="panel-group">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><?php echo $PANEL_LATESTNEWSLETTERS; ?></a>
                </h4>
            </div>

            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                 <th>#</th>
                  <th><?php echo $ROW_SUBJECT; ?></th>
                  <th><?php echo $ROW_USER; ?></th>
		  <th><?php echo $ROW_CREATEDAT; ?></th>
		  <th><?php echo $ROW_CAMPAIGNDATE; ?></th>
                </tr>
              </thead>
              <tbody>
                <?php 
		
		set_include_path(get_include_path() . PATH_SEPARATOR . 'helpers');
		require_once("db_connect.php");
		
		$result = $db->query("SELECT * FROM newsletters ORDER BY id DESC  LIMIT 5");
		while ($Row = $result->fetchRow(DB_FETCHMODE_ASSOC)) {
		       $id            = $Row['id'];
		       $subject       = $Row['subject'];
		       $author        = $Row['author'];
		       $created_at    = $Row['created_at'];
		       $campaign_date = $Row['campaign_date'];
		       
		       echo '<tr>';
		       echo '<td>'.$id.'</td>';
		       echo '<td>'.$subject.'</td>';
		       echo '<td>'.$author.'</td>';
		       echo '<td>'.$created_at.'</td>';
		       echo '<td>'.$campaign_date.'</td>';
		       echo '<td><a href="views/newsletters/edit.php?id='.$id.'"" class="btn btn-small btn-info">'.$ACTIONS_EDIT.'</a> 
			     <a href="views/newsletters/delete.php?id='.$id.'" class="btn btn-danger remove_fields">'.$ACTIONS_DELETE.'</a>
			     <a href="views/newsletters/viewhtml.php?id='.$id.'"" class="btn btn-small btn-info">'.$ACTIONS_VIEWHTML.'</a>
			     <a href="views/newsletters/send.php?id='.$id.'"" class="btn btn-success">'.$ACTIONS_SEND.'</a>
			     </td>';
		       echo '</tr>';
	        }
    
		$result->free();

	  ?>
           
              </tbody>
            </table>
          </div>    
		</div>
            </div>
        </div>
	<!------------------------- LATEST NEWSLETTERS ------------------------------>  
	
	   <!--<h2 class="sub-header">Τελευταία Newsletters</h2>-->
	  
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="libraries/bootstrap/js/jquery.js"></script>
    <script src="libraries/bootstrap/js/bootstrap.js"></script>
    <script src="libraries/bootstrap/js/docs.js"></script>
  

<div data-original-title="Copy to clipboard" title="" style="position: absolute; left: 0px; top: -9999px; width: 15px; height: 15px; z-index: 999999999;" class="global-zeroclipboard-container" id="global-zeroclipboard-html-bridge">      <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="global-zeroclipboard-flash-bridge" height="100%" width="100%">         <param name="movie" value="/assets/flash/ZeroClipboard.swf?noCache=1404936983864">         <param name="allowScriptAccess" value="sameDomain">         <param name="scale" value="exactfit">         <param name="loop" value="false">         <param name="menu" value="false">         <param name="quality" value="best">         <param name="bgcolor" value="#ffffff">         <param name="wmode" value="transparent">         <param name="flashvars" value="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com">         <embed src="libraries/bootstrap/ZeroClipboard.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" name="global-zeroclipboard-flash-bridge" allowscriptaccess="sameDomain" allowfullscreen="false" type="application/x-shockwave-flash" wmode="transparent" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="trustedOrigins=getbootstrap.com%2C%2F%2Fgetbootstrap.com%2Chttp%3A%2F%2Fgetbootstrap.com" scale="exactfit" height="100%" width="100%">                </object></div></body></html>