<?php
/**
 * This file implements the login screen.
 *
 * @package    Pizzafun
 * @subpackage Main
 * @author     Christos Tsiavos <christsiav@gmail.com>
 */
?>
 
<!DOCTYPE html>
<html lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Christos Tsiavos">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Pizzafun Newsletter</title>

    <!-- Include latest jquery -->
    <script src="libraries/bootstrap/js/jquery.js"></script>
    
    <!-- Bootstrap core CSS -->
    <link href="libraries/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="libraries/bootstrap/css/signin.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="libraries/bootstrap/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="libraries/bootstrap/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
    
      <form class="form-signin" role="form" id="form-login">
	<div style="width:100%; text-align:center;"><img src="assets/images/logo-pizzafan.png" style="max-height:100px;"/></div>
        <h2 class="form-signin-heading">Please sign in</h2>
        <input class="form-control" placeholder="Email address" required="" autofocus="" type="email" id="email">
        <input class="form-control" placeholder="Password" required="" type="password" id="password">
        <div class="checkbox">
          <label>
            <input value="remember-me" type="checkbox"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit" id="submitButton">Είσοδος</button>
      </form>
      
    </div> <!-- /container -->

    <!----------------- SIMPLE USERNAME/PASSWORD VALIDATION -------------------->
    <script type="text/javascript">
     
     //prevent default form submission
     $("#form-login").submit(function(e){
	e.preventDefault();
     });
     
     $('#submitButton').click( function() {
     
	var username = $('#email').val();
	var password = $('#password').val();
	
	if (username == 'test@pizzafun.gr' && password == 'test@pizzafun.gr') {
		$('.alert').hide();
		var url = "dashboard.php";    
		$(location).attr('href',url);
	} else
		$('.alert').show();
     });
     </script>
     
    <div class="alert alert-warning" style="display:none">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Σφάλμα!</strong> Μη έγκυρο όνομα ή κωδικός χρήστη. Προσπαθήστε ξανά.
    </div>
    <!----------------- SIMPLE USERNAME/PASSWORD VALIDATION -------------------->
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  

</body></html>